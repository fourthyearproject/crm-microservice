package http

import (
	"../../config"
	"../../keys"
	"encoding/json"
	"fmt"
	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
	"github.com/rs/cors"
	"net/http"
	"os"
	"strings"
)

var r = mux.NewRouter()

func ApiKeyValidatorMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		api_key := r.Header.Get("X-api-key")

		if 0 != strings.Compare(api_key, keys.API_KEY) {
			w.WriteHeader(http.StatusForbidden)
			json.NewEncoder(w).Encode([1]string{"Invalid API key"})
			return
		}
		next.ServeHTTP(w, r)
	})
}

// Validate jwt token middleware
func AuthRequired(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		// Read the token from request Authorization header
		token := r.Header.Get("Authorization")
		if token != "" {
			url := fmt.Sprintf("%svalidate/token", config.AuthMsURL)

			client := &http.Client{}
			req, _ := http.NewRequest("GET", url, nil)
			req.Header.Add("X-api-key", keys.AUTH_API_KEY)
			req.Header.Add("Authorization", token)
			response, _ := client.Do(req)

			if response.StatusCode != 200 {
				w.WriteHeader(http.StatusUnauthorized)
				json.NewEncoder(w).Encode("Unauthorized")
				return
			}
		} else {
			w.WriteHeader(http.StatusUnauthorized)
			json.NewEncoder(w).Encode("Unauthorized")
			return
		}

		next.ServeHTTP(w, r)
	})
}

func Init() {

	r.HandleFunc("/status", func(writer http.ResponseWriter, request *http.Request) {
		status := &struct {
			Name        string `json:"name"`
			Status      string `json:"status"`
			Description string `json:"description"`
		}{Name: config.Name, Status: "Up and running", Description: config.Description}

		writer.Header().Set("Content-type", "application/json")
		json.NewEncoder(writer).Encode(status)
	})

	c := cors.New(cors.Options{
		AllowedOrigins: []string{"*"},
		AllowedMethods: []string{"PUT", "GET", "POST"},
		AllowedHeaders: []string{"X-Api-Key", "Authorization", "Content-Type"},
		Debug:          false,
	})
	r.Use(ApiKeyValidatorMiddleware)
	r.Use(AuthRequired)

	// Insert the middleware
	handler := c.Handler(r)

	http.ListenAndServe(":3001", handlers.CombinedLoggingHandler(os.Stdout, handler))
}
