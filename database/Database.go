package database

import (
	"fmt"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
)

var DB gorm.DB

func Connect() gorm.DB {
	defer recover()
	db, err := gorm.Open("mysql", "root:root@/purchase_inventory?charset=utf8&parseTime=True&loc=Local")

	if err != nil {
		panic(err.Error())
	}

	fmt.Println(db.DB().Ping())

	//defer db.Close()
	return *db

}

func CloseConnection(db gorm.DB) {
	db.Close()
}
