package migrations

import (
	"github.com/jinzhu/gorm"
)


type Role struct {
	gorm.Model
	Name        string `json:"name"`
	Description string `json:"description"`
}

type User struct {
	gorm.Model
	FName     string `json:"f_name"`
	LName     string `json:"l_name"`
	Email     string `json:"email"`
	Phone     string `json:"phone"`
	Confirmed bool   `son:"confirmed"`
	Role      Role   `json:"role"`
	RoleID    uint   `json:"role_id"`
}
