package migrations

import "../database"

func Migrate() {

	db := database.Connect()

	db.AutoMigrate()

}

func DropTables() {

	db := database.Connect()

	db.DropTableIfExists()

	database.CloseConnection(db)
}
