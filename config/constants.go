package config

const (
	Name        = "Customer Relationship Module"
	Description = "Customer Relationship Module Micro service"

	AuthMsURL = "http://localhost:8181/"

	SUPER_ADMIN_ID  = 1
	ADMIN_ID        = 2
	HR_ID           = 3
	STORE_KEEPER_ID = 4
	SUPPLIER_ID     = 5
	USER_CLIENT     = 6
)
